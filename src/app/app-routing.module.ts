import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DrugstoreComponent } from './drugstore/drugstore.component';


const routes: Routes = [
  { path: '',     component: DrugstoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
