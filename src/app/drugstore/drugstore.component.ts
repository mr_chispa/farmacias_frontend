import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MapmodalComponent } from '../mapmodal/mapmodal.component';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-drugstore',
  templateUrl: './drugstore.component.html',
  styleUrls: ['./drugstore.component.css']
})
export class DrugstoreComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  regions: string[];
  drugStores: any[];
  dataSource = new MatTableDataSource([]);
  displayedColumns: string[] = ['commune', 'store', 'address', 'phone', 'lat', 'long', 'latlng'];

  constructor(private restApiService: RestApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.restApiService.getRegions()
    .subscribe(
      (data) => { // Success
        this.regions = data;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  regionChange(selectedVal){
    this.restApiService.getDrugStores(selectedVal)
    .subscribe(
      (data) => { // Success
        //this.drugStores = data;
        this.dataSource = new MatTableDataSource(data);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  showMap(lat: string, lng: string) {
    this.dialog.open(MapmodalComponent, {
      data: {
        lat,
        lng
      },
      height: '400px',
      width: '600px',
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
