import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-mapmodal',
  templateUrl: './mapmodal.component.html',
  styleUrls: ['./mapmodal.component.css']
})
export class MapmodalComponent implements OnInit {

  lat: any;
  lng: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.lat = parseFloat(data.lat);
    this.lng = parseFloat(data.lng);
  }

  ngOnInit() {
  }

}
